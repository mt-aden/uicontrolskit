//
//  Date+Formatting.swift
//  SeatTicket
//
//  Created by Alok Choudhary on 6/23/18.
//  Copyright © 2018 Mt Aden LLC. All rights reserved.
//

import Foundation

extension String {
    static let iso8601DateFormat: String = "yyyy-MM-dd'T'HH:mm:ssZ"
    static let modifiedISO8601Format: String = "yyyy-MM-dd'T'HH:mm:ss"
    static let defaultDateFormat: String = "MM/dd/yyyy"
    static let amadeusDateFormat: String = "yyyy-MM-dd"
    static let weekDayDateMonthYearTimeAMPM: String = "dd MMM yyyy h:mm a"
}

extension Date {
    func convert(using format: String = .defaultDateFormat, timeZone: TimeZone = .current) -> Date? {
        let formatter = dateFormatter(with: format, timeZone: timeZone)
        let dateString = formatter.string(from: self)
        return formatter.date(from: dateString)
    }
    
    func format(_ format: String = .defaultDateFormat, timeZone: TimeZone = .current) -> String {
        let formatter = dateFormatter(with: format, timeZone: timeZone)
        return formatter.string(from: self)
    }
    
    func fullDayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).capitalized
    }
    
    func shortDayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE"
        return dateFormatter.string(from: self).uppercased()
    }
}

extension String {
    func changeDateFormat(to format: String, timeZone: TimeZone = .current) -> String? {
        let formatter = dateFormatter(with: format, timeZone: timeZone)
        guard
            let date = formatter.date(from: self)
            else { return nil }
        return formatter.string(from: date)
    }
    
    func date(with format: String = .defaultDateFormat, timeZone: TimeZone = .current) -> Date? {
        let formatter = dateFormatter(with: format, timeZone: timeZone)
        return formatter.date(from: self)
    }
    
    func showFormattedDateWithDayOfWeek() -> String? {
        let date = self.date(with: .modifiedISO8601Format, timeZone: .current)
        let dayOfWeek = date?.shortDayOfWeek()
        return "\(dayOfWeek ?? ""), \(date?.format(.weekDayDateMonthYearTimeAMPM, timeZone: .current) ?? "")"
    }
}

private func dateFormatter(with format: String, timeZone: TimeZone) -> DateFormatter {
    let formatter = DateFormatter()
    formatter.timeZone = timeZone
    formatter.dateFormat = format
    return formatter
}
