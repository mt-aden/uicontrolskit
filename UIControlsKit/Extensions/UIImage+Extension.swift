//
//  UIImage+Extension.swift
//  SeatTicket
//
//  Created by Alok Choudhary on 6/23/18.
//  Copyright © 2018 Mt Aden LLC. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

public extension UIImageView {
    func loadImage(_ url : URL?) {
        self.kf.indicatorType = .activity
        self.kf.setImage(with: url)
    }
    
    public func loadImageResource(string urlStr : String?, placeholder: UIImage?, cacheKey: String?) {
        guard
            let str = urlStr,
            let imgUrl = URL(string: str)
            else { return }
        let resource = ImageResource(downloadURL: imgUrl, cacheKey: cacheKey)
        self.kf.indicatorType = .activity
        self.kf.setImage(with: resource, placeholder: placeholder, options: nil, progressBlock: nil, completionHandler: nil)
    }
    
    public func loadImage(_ url : String?) {
        guard let urlStr = url else {return}
        self.kf.setImage(with: URL.init(string: urlStr))
    }
}
