# UIControlsKit
[![Swift Version][swift-image]][swift-url]
[![Build Status][travis-image]][travis-url]
[![License][license-image]][license-url]
[![Carthage compatible](https://img.shields.io/badge/Carthage-compatible-4BC51D.svg?style=flat)](https://github.com/Carthage/Carthage)
[![Platform](https://img.shields.io/cocoapods/p/LFAlertController.svg?style=flat)](http://cocoapods.org/pods/LFAlertController)
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com)

> Usable custom controls that is baked for my personal needs.

![](header.png)

## Features

- [x] Feature 1
- [x] Feature 2
- [x] Feature 3
- [x] Feature 4
- [x] Feature 5

## Requirements

- iOS 10.0+
- Xcode 9.0

## Installation

#### Carthage
Create a `Cartfile` that lists the framework and run `carthage update`. Follow the [instructions](https://github.com/Carthage/Carthage#if-youre-building-for-ios) to add `$(SRCROOT)/Carthage/Build/iOS/YourLibrary.framework` to an iOS project.

```
github "alokc83/UIControlsKit"
```
if using bitbucket repo 
```
git https://bitbucket.org/mt-aden/uicontrolskit/
```

#### Manually (may not be manual way) 
1. Download and drop ```YourLibrary.swift``` in your project.  
2. Congratulations!  

#### Dependencies: 
* github "onevcat/Kingfisher" (This is amazing library based on SDWebImage)

## Usage example

```swift
import UIControlsKit
ez.detectScreenShot { () -> () in
    print("User took a screen shot")
}
```

## Contribute

We would love you for the contribution to **UIControlsKit**, check the ``LICENSE`` file for more info.

## Meta

Alok C � [@alokc83](https://twitter.com/dbader_org) � 

Distributed under the MIT license. See ``LICENSE`` for more information.

[https://bitbucket.org/mt-aden/uicontrolskit/](https://bitbucket.org/mt-aden/uicontrolskit/)

[swift-image]:https://img.shields.io/badge/swift-4.0-orange.svg
[swift-url]: https://swift.org/
[license-image]: https://img.shields.io/badge/License-MIT-blue.svg
[license-url]: LICENSE
[travis-image]: https://img.shields.io/travis/dbader/node-datadog-metrics/master.svg?style=flat-square
[travis-url]: https://travis-ci.org/dbader/node-datadog-metrics
[codebeat-image]: https://codebeat.co/badges/c19b47ea-2f9d-45df-8458-b2d952fe9dad
[codebeat-url]: https://codebeat.co/projects/github-com-vsouza-awesomeios-com