//
//  UIColor+Extension.swift
//  SeatTicket
//
//  Created by Alok Choudhary on 6/25/18.
//  Copyright © 2018 Mt Aden LLC. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    public convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let redMask = Int(color >> 16) & mask
        let greenMask = Int(color >> 8) & mask
        let blueMask = Int(color) & mask
        let red   = CGFloat(redMask) / 255.0
        let green = CGFloat(greenMask) / 255.0
        let blue  = CGFloat(blueMask) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    public func toHexString() -> String {
        var redFloat:CGFloat = 0
        var greenFloat:CGFloat = 0
        var blueFloat:CGFloat = 0
        var alphaFloat:CGFloat = 0
        getRed(&redFloat, green: &greenFloat, blue: &blueFloat, alpha: &alphaFloat)
        let rgb:Int = (Int)(redFloat*255)<<16 | (Int)(greenFloat*255)<<8 | (Int)(blueFloat*255)<<0
        return String(format:"#%06x", rgb)
    }
}
