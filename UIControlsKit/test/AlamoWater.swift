//
//  AlamoWater.swift
//  UIControlsKit
//
//  Created by Alok Choudhary on 6/26/18.
//  Copyright © 2018 Mt Aden LLC. All rights reserved.
//

import UIKit

public protocol AlamoWaterProtocol {
    func didCallHello()
}

open class AlamoWater: NSObject {
    public static let shared = AlamoWater()
    
    public var delegate:AlamoWaterProtocol?
    
    open func hello(){
        debugPrint("Hello from AlamoWater!")
        AlamoWater.shared.delegate?.didCallHello()
    }
}
