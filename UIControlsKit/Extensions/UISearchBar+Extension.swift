//
//  UISearchBar+Extension.swift
//  SeatTicket
//
//  Created by Alok Choudhary on 6/25/18.
//  Copyright © 2018 Mt Aden LLC. All rights reserved.
//

import Foundation
import UIKit

public extension UISearchBar {
    public func setTextColor(color: UIColor) {
        let svs = subviews.flatMap { $0.subviews }
        guard let tf = (svs.filter { $0 is UITextField }).first as? UITextField else { return }
        tf.textColor = color
    }
    
    public var textField: UITextField? {
        let subViews = subviews.flatMap { $0.subviews }
        guard let textField = (subViews.filter { $0 is UITextField }).first as? UITextField else { return nil
        }
        return textField
    }
}
